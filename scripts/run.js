const main = async () => {
  
    const [owner] = await hre.ethers.getSigners();

    const gameContractFactory = await hre.ethers.getContractFactory('dfsTeamMinter');
    const gameContract = await gameContractFactory.deploy();
    await gameContract.deployed();
    console.log("Contract deployed to:", gameContract.address);

    await gameContract.refreshAndPayout(1852889571, 0, [])
    await gameContract.refreshAndPayout(1852889571, 1, [])


    let txn, txn_two, txn_three, txn_four, txn_five, txn_six, txn_eight, txn_nine, txn_ten, txn_eleven, txn_twelve;

    txn = await gameContract.mintDFSTeamNFT(
        1653066959,
        {value: ethers.utils.parseEther("1")}
    );
    
    await txn.wait();

    txn_two = await gameContract.mintDFSTeamNFT(
      1653066959,
      {value: ethers.utils.parseEther("1")}
    );

    await txn_two.wait();

    txn_three = await gameContract.mintDFSTeamNFT(
      1653066959,
      {value: ethers.utils.parseEther("1")}
    );

    await txn_three.wait();

    txn_four = await gameContract.mintDFSTeamNFT(
      1653066959,
      {value: ethers.utils.parseEther("1")}
    );
  
    await txn_four.wait();

    txn_five = await gameContract.mintDFSTeamNFT(
      1653066959,
      {value: ethers.utils.parseEther("1")}
    );
  
    await txn_five.wait();

    txn_six = await gameContract.mintDFSTeamNFT(
      1653066959,
      {value: ethers.utils.parseEther("1")}
    );
  
    await txn_six.wait();

    let ownerAddress = await gameContract.ownerOf(1)

    //console.log(ownerAddress)

    let token = await gameContract.tokenOfOwnerByIndex(ownerAddress, 0)
    let token_two = await gameContract.tokenOfOwnerByIndex(ownerAddress, 1)
    let token_three = await gameContract.tokenOfOwnerByIndex(ownerAddress, 2)

    const parsed = parseInt(token._hex);
    const parsed_two = parseInt(token_two._hex);
    const parsed_three = parseInt(token_three._hex);

    console.log("UserToken #2: ", parsed_two)

    await gameContract.stakeTeamInContest(parsed_two, 0)


   //  TESTING STAKE TEAM IN CONTEST
    await gameContract.stakeTeamInContest(parsed, 0)
    await gameContract.stakeTeamInContest(parsed_three, 0)
    await gameContract.stakeTeamInContest(4, 0)
    await gameContract.stakeTeamInContest(5, 0)
    await gameContract.stakeTeamInContest(6, 0)

    let contestTokens = await gameContract.getAllContestTokens(0)

    console.log("Contest Tokens Original: ", contestTokens)

    await gameContract.mintAndStakeTeamNFT(1653066959, 1,{value: ethers.utils.parseEther("1")})
    await gameContract.mintAndStakeTeamNFT(1653066959, 1,{value: ethers.utils.parseEther("1")})
    await gameContract.mintAndStakeTeamNFT(1653066959, 1,{value: ethers.utils.parseEther("1")})
    await gameContract.mintAndStakeTeamNFT(1653066959, 1,{value: ethers.utils.parseEther("1")})
    await gameContract.mintAndStakeTeamNFT(1653066959, 1,{value: ethers.utils.parseEther("1")})
    await gameContract.mintAndStakeTeamNFT(1653066959, 1,{value: ethers.utils.parseEther("1")})

    contestTokens = await gameContract.getAllContestTokens(1)

    console.log("Contest Tokens Contest 2: ", contestTokens)

    await gameContract.setTokenScore(0,1,325)
    await gameContract.setTokenScore(0,2,323)
    await gameContract.setTokenScore(0,3,286)
    await gameContract.setTokenScore(0,4,354)
    await gameContract.setTokenScore(0,5,245)
    await gameContract.setTokenScore(0,6,333)

    contestTokens = await gameContract.getAllContestTokens(0)

    console.log("Contest 1 Tokens Before Payout: ", contestTokens)
    let balance = await hre.ethers.provider.getBalance(gameContract.address)
    console.log("Contract Balance: ", balance )

    let playerBalance = await hre.ethers.provider.getBalance(owner.address)
    console.log("Owner Balance: ", playerBalance)

    await gameContract.refreshAndPayout(1644120815, 0, [50,50])

    contestTokens = await gameContract.getAllContestTokens(0)

    balance = await hre.ethers.provider.getBalance(gameContract.address)
    console.log("Contract 1 Balance after payout: ", balance )
    
    playerBalance = await hre.ethers.provider.getBalance(owner.address)
    console.log("Owner Balance after payout: ", playerBalance)

    console.log("Contest 1 Tokens: ", contestTokens)

    await gameContract.setTokenScore(1,7,325)
    await gameContract.setTokenScore(1,8,323)
    await gameContract.setTokenScore(1,9,286)
    await gameContract.setTokenScore(1,10,354)
    await gameContract.setTokenScore(1,11,245)
    await gameContract.setTokenScore(1,12,333)

    contestTokens = await gameContract.getAllContestTokens(1)
    

    console.log("Contest 2 Tokens Before Payout: ", contestTokens)
    balance = await hre.ethers.provider.getBalance(gameContract.address)
    console.log("Contract Balance: ", balance )
    playerBalance = await hre.ethers.provider.getBalance(owner.address)
    console.log("Owner Balance: ", playerBalance)

    await gameContract.refreshAndPayout(1644120815, 1, [75,15, 5, 5])

    contestTokens = await gameContract.getAllContestTokens(1)

    balance = await hre.ethers.provider.getBalance(gameContract.address)
    console.log("Contract Balance after payout: ", balance )
    
    playerBalance = await hre.ethers.provider.getBalance(owner.address)
    console.log("Owner Balance after payout: ", playerBalance)

    console.log("Contest 2 Tokens: ", contestTokens)
    
    await gameContract.withdrawFunds()

    balance = await hre.ethers.provider.getBalance(gameContract.address)
    console.log("Contract Balance after withdraw: ", balance )

    playerBalance = await hre.ethers.provider.getBalance(owner.address)
    console.log("Owner Balance after Withdraw: ", playerBalance)

  };
  
  const runMain = async () => {
    try {
      await main();
      process.exit(0);
    } catch (error) {
      console.log(error);
      process.exit(1);
    }
  };
  
  runMain();