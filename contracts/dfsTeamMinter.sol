// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

// Helper we wrote to encode in Base64
import "./libraries/Base64.sol";

import "hardhat/console.sol";

// NFT contract to inherit from.
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
//need Ennumerable for the tokenOfOwnerByIndex function.
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";

// Helper functions OpenZeppelin provides.
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/Strings.sol";



contract dfsTeamMinter is ERC721Enumerable {

      // The tokenId is the NFTs unique identifier, it's just a number that goes
    // 0, 1, 2, 3, etc.
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;

    uint256 constant CONTEST_FEE = 1 ether; //One "Ether" on Harmony = one $ONE
    
    struct dfsTeam {

        uint mintDate;
        uint score;
        bool isStaked;
        uint amountWon;
    } 

    uint256[5] lockTime; //hold todays lock time.

    //mapping from tokenId to users address.
    mapping(uint256 => address) public tokenAddresses;

      // We create a mapping from the nft's tokenId => that NFTs attributes. Can I set this as private?
    mapping(uint256 => dfsTeam) public nftHolderAttributes;

    // A mapping from an address => the NFTs tokenId. Gives me an ez way
    // to store the owner of the NFTs and reference it later.
    mapping(address => uint256[]) public nftHolders;

    // mapping from the ContestId -> an array of the tokens in the contests.
    mapping(uint256 => uint256[]) public contestTokens;

    event dfsTeamNFTMinted(address sender, uint256 tokenId); 
    event nftStakedInContest(address staker, uint256 tokenId); 
    event contestWinners( uint[] winners, uint payout); 

    //Requests to change need to be made from aiSports dev account - will need to change this to the aisports dev account address
    //address aiSportsRequest = 0x3e4a729E0A975d61494f6992aB19271167D8E0AE; //production
    address aiSportsRequest = 0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266; //local development


    // Below, you can also see I added some special identifier symbols for our NFT.
    // This is the name and symbol for our token, ex Ethereum and ETH. 
    // Remember, an NFT is just a token!
    constructor() ERC721("aiSports", "AIS") payable {
        console.log("Yo yo, I am a contract and I am Minting a DFS team");

        //we may use this later on to make the contest, or use another contract

        // I increment tokenIds here so that my first NFT has an ID of 1.
        // More on this in the lesson!
        _tokenIds.increment();
    }

    // Users would be able to hit this function and get their NFT based on the
  // the dfsTeam they submit
  function mintDFSTeamNFT(
        uint _mintDate
      ) external payable {

    require (
      msg.value >= CONTEST_FEE,
      "Error: not enough ETH to Mint a team."
    );

    // Get current tokenId (starts at 1 since we incremented in the constructor).
    uint256 newItemId = _tokenIds.current();

    // The magical function! Assigns the tokenId to the caller's wallet address.
    _safeMint(msg.sender, newItemId);

    // We map the tokenId => their character attributes. 
    nftHolderAttributes[newItemId] = dfsTeam({
        mintDate: _mintDate,
        score: 0,
        isStaked: false,
        amountWon: 0
    });

    console.log("Minted NFT w/ tokenId %s for Date %s", newItemId, _mintDate);
    
    // Keep an easy way to see who owns which NFTs.
    nftHolders[msg.sender].push(newItemId);

    // Increment the tokenId for the next person that uses it.
    _tokenIds.increment();

    emit dfsTeamNFTMinted(msg.sender, newItemId);
  }

  function mintAndStakeTeamNFT( uint _mintDate, uint256 _contestId ) external payable {

    require (
      msg.value >= CONTEST_FEE,
      "Error: not enough ETH to Mint a team."
    );

    //make sure we are not past lockTime - this needs to be checked with frontend code.
    if(lockTime[_contestId] > 0) {
      require(block.timestamp <= lockTime[_contestId], "Passed lock time");
    }
    // Get current tokenId (starts at 1 since we incremented in the constructor).
    uint256 newItemId = _tokenIds.current();

    //Assigns the tokenId to the caller's wallet address.
    _safeMint(msg.sender, newItemId);


    // We map the tokenId => their character attributes. 
    nftHolderAttributes[newItemId] = dfsTeam({
        mintDate: _mintDate,
        score: 0,
        isStaked: true,
        amountWon: 0
    });

    console.log("Minted NFT w/ tokenId %s for Date %s", newItemId, _mintDate);
    
    // Keep an easy way to see who owns which NFTs.
    nftHolders[msg.sender].push(newItemId);

    // Increment the tokenId for the next person that uses it.
    _tokenIds.increment();

    tokenAddresses[newItemId] = msg.sender;
    contestTokens[_contestId].push(newItemId); 

    console.log("Contest Entered");


    emit nftStakedInContest(msg.sender, newItemId);


  }

  function getNFTdataByToken(uint256 _tokenId) public view returns (uint[3] memory) {
    
    dfsTeam memory dfsAttributes = nftHolderAttributes[_tokenId];

    //I may want to also add the isStaked Variable here to read

    uint[3] memory playerArray = [
      dfsAttributes.mintDate,
      dfsAttributes.score,
      dfsAttributes.amountWon
    ];

    return playerArray;

  }

  function withdrawFunds() public {

    require(msg.sender == aiSportsRequest, "only aiSports owner can call this function"); //confirm that this is coming from an aiSports account

    (bool success, ) = address(aiSportsRequest).call{value: address(this).balance}("");
    require(success, "Failed to withdraw balance from contract.");

  }

  function stakeTeamInContest(uint256 _tokenId, uint256 _contestId) public { //stake the team in the contest, return true if successful.
    
    //add to the team mapping
    //used require below to make sure that the owner owns the token. //msg.sender 

    //make sure we are not past lockTime - this needs to be fixed/checked.
    if(lockTime[_contestId] > 0){
      require(block.timestamp <= lockTime[_contestId], "Passed lock time");
    }

    require (nftHolderAttributes[_tokenId].isStaked == false, "Team has already been staked");

    //Do I need another require to make sure that the team is in todays contest?
    
    uint256[] memory nftArray = nftHolders[msg.sender];

    bool hasNFT = false;

    for (uint i; i < nftArray.length; i++) { //this may take alot of resources if the user has alot of NFTs, need to encourage users to burn NFTs or filter nfts by day?
      if(_tokenId == nftArray[i]) {
        hasNFT = true;
      }
    }

    require( hasNFT == true ); //make sure that this address is the owner of the NFT, otherwise anyone could enter it in the contest

    nftHolderAttributes[_tokenId].isStaked = true;

    tokenAddresses[_tokenId] = msg.sender;
    contestTokens[_contestId].push(_tokenId); 

    console.log("Contest Entered");

    emit nftStakedInContest(msg.sender, _tokenId);

  }

   function refreshAndPayout(uint256 _lockTime, uint256 _contestId, uint256[] memory _payoutArray) public { 
      
    require(msg.sender == aiSportsRequest, "only aiSports can call this function"); //confirm that this is coming from an aiSports account

    uint totalWinners;
    uint _payout;

    if(contestTokens[_contestId].length > 0){
      (totalWinners, _payout) = calculateWinnersAndSendPayouts(_contestId, _payoutArray);
    } else {
      totalWinners = 0;
      _payout = 0;
    }

    uint[] memory _winnerArray = new uint[](totalWinners);

    for (uint i = 0; i < totalWinners; i++) {
      _winnerArray[i] = contestTokens[_contestId][i];
    }    

    clearContestAndSetLock(_lockTime, _contestId);

    emit contestWinners(_winnerArray, _payout);

   }

  function calculateWinnersAndSendPayouts(uint256 _contestId, uint256[] memory _payoutArray) private returns (uint, uint){ 

    uint totalPayout = contestTokens[_contestId].length * CONTEST_FEE; //need to make sure the tokensInContest resets to 0
    totalPayout = totalPayout - (totalPayout/50); //contract keeps 2 percent for running the contest.

    require(
      totalPayout <= address(this).balance,
      "Trying to withdraw more money than the contract has."
    );

    if(_payoutArray[0] == 50 && _payoutArray[1] == 50 ){ //if this is a 50-50 contest. Will likely change this to a more robust system.

      uint totalWinners = (contestTokens[_contestId].length / 2 + contestTokens[_contestId].length % 2);

      while((totalWinners < contestTokens[_contestId].length) && (nftHolderAttributes[contestTokens[_contestId][totalWinners-1]].score == nftHolderAttributes[contestTokens[_contestId][totalWinners]].score)){
          totalWinners++; //increase the amount of winners if they have the same score as the median
      }

      uint userPayout = totalPayout/totalWinners;

      //send eth to all winners
      for(uint i = 0; i < totalWinners; i++){

        address winner = tokenAddresses[contestTokens[_contestId][i]];
        (bool success, ) = (winner).call{value: userPayout}("");
        require(success, "Failed to withdraw money from contract.");
      }

      return (totalWinners, userPayout);

    } else {

      uint totalWinners = _payoutArray.length;

      for(uint i = 0; i < totalWinners; i++){

        uint userPayout = _payoutArray[i] * totalPayout / 100;

        console.log(userPayout);

        address winner = tokenAddresses[contestTokens[_contestId][i]];
        (bool success, ) = (winner).call{value: userPayout}("");
        require(success, "Failed to withdraw money from contract.");

      }


      return (0,0);

    }

  }

  function clearContestAndSetLock(uint256 _lockTime, uint256 _contestId) private { 

    for (uint i = 0; i < contestTokens[_contestId].length; i++) {
      delete tokenAddresses[contestTokens[_contestId][i]];
    }

    delete contestTokens[_contestId];

    //set todays locktime
    lockTime[_contestId] = _lockTime;

  }

  function getAllContestTokens(uint256 _contestId) public view returns(uint256[] memory){
    return contestTokens[_contestId];
  }

  function setTokenScore(uint256 _contestId, uint256 _tokenId, uint256 _score) public {

    require(msg.sender == aiSportsRequest, "only aiSports can call this function"); //confirm that this is coming from an aiSports account
    require(nftHolderAttributes[_tokenId].isStaked == true, "token needs to be staked to set score");  

    nftHolderAttributes[_tokenId].score = _score;
    bool _pushedTokenToArray = false;
    uint[] memory _sortedArray = new uint[](contestTokens[_contestId].length);
    uint j = 0;

    //loop through the tokensInContest and place the token in its correct place.
    for (uint i = 0; i < contestTokens[_contestId].length; i++) {

      if(contestTokens[_contestId][i] == _tokenId){ //if i is the location of the current token, and the token has not yet been pushed to array, then add it.

        if(!_pushedTokenToArray){
          _sortedArray[j] = _tokenId;
          _pushedTokenToArray = true;
          j++;
        }

      } else if((!_pushedTokenToArray) && (nftHolderAttributes[contestTokens[_contestId][i]].score < _score) ){ //if the current token is less than the token at position I, and the token has not been pushed to the array, then add it.

        _pushedTokenToArray = true;

        _sortedArray[j] = _tokenId;
        _sortedArray[++j] = contestTokens[_contestId][i];
        j++;
        
      } else {
        _sortedArray[j] = contestTokens[_contestId][i];
        j++;
      }
    }

    contestTokens[_contestId] = _sortedArray;

  }

}


  /*
  OLD & TEST FUNCTIONS

      //display all tokens in contest - TEST FUNCTION
  function loopThroughContest() public view {

    for(uint i = 0; i < tokensInContest.length; i++){
      console.log(tokensInContest[i]);
      console.log(fiftyFiftyTokenIds[tokensInContest[i]]);
    }

    console.log("Array Length: ", tokensInContest.length);
    console.log(fiftyFiftyTokenIds[1]);
    
  }

    //could add this function if using opensea
  function tokenURI(uint256 _tokenId) public view override returns (string memory) {
    dfsTeam memory dfsAttributes = nftHolderAttributes[_tokenId];

    string memory contestDate = dfsAttributes.contestDate;
    //uint pgOneId = dfsAttributes.pgOnePlayerId;
    //uint pgTwoId = dfsAttributes.pgTwoPlayerId;
    uint[9] memory playerArray = [
      dfsAttributes.pgOnePlayerId,
      dfsAttributes.pgTwoPlayerId,
      dfsAttributes.sgOnePlayerId,
      dfsAttributes.sgTwoPlayerId,
      dfsAttributes.sfOnePlayerId,
      dfsAttributes.sfTwoPlayerId,
      dfsAttributes.pfOnePlayerId,
      dfsAttributes.pfTwoPlayerId,
      dfsAttributes.cPlayerId
    ];

    string memory imageURI = "https://images.pexels.com/photos/4562470/pexels-photo-4562470.jpeg?cs=srgb&dl=pexels-jeandaniel-francoeur-4562470.jpg&fm=jpg";

    string memory json = Base64.encode(
      bytes(
        string(
                abi.encodePacked(
                '{"Contest Date": "',
                contestDate,
                '","NFT #": "',
                Strings.toString(_tokenId),
                '", "description": "This is an aiSports NFT, it can be used to enter the contests today", "image": "',
                imageURI,
                '", "attributes": [ { "trait_type": "PG 1", "value": ',Strings.toString(playerArray[0]),'}, { "trait_type": "PG 2", "value": ',Strings.toString(playerArray[1]),'}, { "trait_type": "SG 1", "value": ', Strings.toString(playerArray[2]),'}, { "trait_type": "SG 2", "value": ',Strings.toString(playerArray[3]),'}, { "trait_type": "SF 1", "value": ',Strings.toString(playerArray[4]),'}, { "trait_type": "SF 2", "value": ',Strings.toString(playerArray[5]),'}, { "trait_type": "PF 1", "value": ',Strings.toString(playerArray[6]),'}, { "trait_type": "PF 2", "value": ',Strings.toString(playerArray[7]),'}, { "trait_type": "C", "value": ',
                Strings.toString(playerArray[8]),'} ]}'
                )
        )
      )
    );

    string memory output = string(
      abi.encodePacked("data:application/json;base64,", json)
    );
    
    return output;
  }

    //CAN COMMENT OUT FOR PROD - needed for testing
  function getFiftyFiftyTokenAddr(uint256 _tokenId) public view returns (address) {

    return fiftyFiftyTokenIds[_tokenId];

  }

  //Currently this function just returns the first team the user has. Will need to modify for more teams.
  function checkIfUserHasNFT() public view returns (dfsTeam memory) {

    // Get the tokenId of the user's character NFT
    uint256[] memory userNftTokenIds = nftHolders[msg.sender];

    // If the user has a tokenId in the map, return their team.

    if (userNftTokenIds[0] > 0) {
        return nftHolderAttributes[userNftTokenIds[0]]; //default just sending the first team, but we will need to change this.
    }// Else, return an empty character.
    else {
      dfsTeam memory emptyStruct;
      return emptyStruct;
    }

  } 



  //May use a similar functio as below n to return current contests.

  function getBigBoss() public view returns (BigBoss memory) {
    return bigBoss;
  }
  */
